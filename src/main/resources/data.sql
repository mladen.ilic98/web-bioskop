
INSERT INTO Spectator(username,password,name,surname,role,phone,email,dateOB)
VALUES ('Anaaa','ana','Ana','Minic','spectator','065443321','ana@gmail.com','12.03.2000');
INSERT INTO Spectator(username,password,name,surname,role,phone,email,dateOB)
VALUES ('djomla','djomla','Mladen','Ilic','spectator','065443321','djomla@gmail.com','22.03.1998');
INSERT INTO Spectator(username,password,name,surname,role,phone,email,dateOB)
VALUES ('lazar','lazar','Lazar','Kostic','spectator','0694556227','lazar@gmail.com','14.09.1994');


INSERT INTO Movie(nameOM,description,genre,duration,mark,price)
VALUES ('Anabel','Jako dobar horor film','horror','2h',8.6,150.4);
INSERT INTO Movie(nameOM,description,genre,duration,mark,price)
VALUES ('FastAndFurious1','Jako dobar akcioni film','action','2h',9.0,150);
INSERT INTO Movie(nameOM,description,genre,duration,mark,price)
VALUES ('Baby driver','Jako dobar akcioni film','action','2h',9.1,350);
INSERT INTO Movie(nameOM,description,genre,duration,mark,price)
VALUES ('Kosare','Jako dobar dokumentarni film','documentary','1.5h',9.3,250);

INSERT INTO Auditorium(capacity,markOA)
VALUES (10,'H1');
INSERT INTO Auditorium(capacity,markOA)
VALUES (20,'H2');
INSERT INTO Auditorium(capacity,markOA)
VALUES (30,'H3');
INSERT INTO Auditorium(capacity,markOA)
VALUES (40,'H4');

INSERT INTO Cinema(nameOC,adress,numberOP,email)
VALUES ('Sloboda','Karadjordjeva','017420166','sloboda@gmail.com');
INSERT INTO Cinema(nameOC,adress,numberOP,email)
VALUES ('Okce','29. novembra','017420167','okce@gmail.com');
