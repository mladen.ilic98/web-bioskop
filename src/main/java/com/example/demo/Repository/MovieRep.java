package com.example.demo.Repository;

import com.example.demo.Entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRep extends JpaRepository<Movie,Long> {
    List<Movie> findAllByOrderByNameOMAsc();
    List<Movie> findAllByOrderByNameOMDesc();
    List<Movie> findAllByOrderByPriceAsc();
    List<Movie> findAllByOrderByPriceDesc();


}
