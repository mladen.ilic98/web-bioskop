package com.example.demo.Repository;

import com.example.demo.Entity.Administrator;
import com.example.demo.Entity.Spectator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRep extends JpaRepository<Administrator, Long> {

}
