package com.example.demo.Repository;

import com.example.demo.Entity.Spectator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KorisnikRep extends JpaRepository<Spectator, Long> {
    Spectator findByUsernameAndPassword(String username, String password);
    Spectator findByUsername(String username);
    Spectator findByName(String name);

}
