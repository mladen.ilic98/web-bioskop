package com.example.demo.Service;

import com.example.demo.Entity.Movie;
import com.example.demo.Repository.MovieRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MovieService {
    List<Movie> sortByNameUp();
    List<Movie> sortByNameDown();
    List<Movie> sortByPriceUp();
    List<Movie> sortByPriceDown();
    Movie findById(Long id);

    List<Movie> findAll();
}
