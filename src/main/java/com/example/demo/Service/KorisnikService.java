package com.example.demo.Service;

import com.example.demo.Entity.Korisnik;
import com.example.demo.Entity.Spectator;
import org.springframework.stereotype.Service;

@Service
public interface KorisnikService {
    Spectator findByUsernameAndPassword(String username, String password);
    void deleteAll(Long id);
    Spectator findByUsername(String username);
    Spectator findForRegistration(String name);
    Spectator registration(Spectator spectator)  throws  Exception;
    Spectator findById(Long id);
    Spectator updateSpectaor(Spectator spectator);
}
