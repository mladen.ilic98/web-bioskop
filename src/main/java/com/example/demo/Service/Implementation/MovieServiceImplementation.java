package com.example.demo.Service.Implementation;

import com.example.demo.Entity.Movie;
import com.example.demo.Repository.MovieRep;
import com.example.demo.Service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Service
public class MovieServiceImplementation implements MovieService {
    @Autowired
    MovieRep movieRep;

    @Override
    public List<Movie> sortByNameUp() {
        List<Movie> movies = this.movieRep.findAllByOrderByNameOMAsc();
        return movies;
    }

    @Override
    public List<Movie> sortByNameDown() {
        List<Movie> movies = this.movieRep.findAllByOrderByNameOMDesc();
        return movies;
    }

    @Override
    public List<Movie> sortByPriceUp() {
        List<Movie> movies = this.movieRep.findAllByOrderByPriceAsc();
        return movies;
    }

    @Override
    public List<Movie> sortByPriceDown() {
        List<Movie> movies = this.movieRep.findAllByOrderByPriceDesc();
        return movies;
    }

    @Override
    public Movie findById(Long id) {
        return this.movieRep.getOne(id);
    }

    @Override
    public List<Movie> findAll() {

        return this.movieRep.findAll();

    }


}
