package com.example.demo.Service.Implementation;

import com.example.demo.Entity.Spectator;
import com.example.demo.Repository.KorisnikRep;
import com.example.demo.Service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KorisnikServiceImplementation implements KorisnikService {

    @Autowired
    private KorisnikRep korisnikRep;

    @Override
    public Spectator findByUsernameAndPassword(String username, String password) {
        Spectator s = this.korisnikRep.findByUsernameAndPassword(username,password);
        return s;
    }

    @Override
    public void deleteAll(Long id) {
        Spectator spectator= korisnikRep.getOne(id);
        korisnikRep.delete(spectator);
    }

    @Override
    public Spectator findByUsername(String username) {
        return korisnikRep.findByUsername(username);
    }

    @Override
    public Spectator findForRegistration(String name) {
        Spectator spectator = this.korisnikRep.findByName(name);
        return spectator;
    }

    @Override
    public Spectator registration(Spectator spectator) throws Exception {
        if(korisnikRep.findByUsername(spectator.getUsername()) != null){
            return null;
        }else{
            return korisnikRep.save(spectator);
        }
    }

    @Override
    public Spectator findById(Long id) {
        return korisnikRep.getOne(id);
    }

    @Override
    public Spectator updateSpectaor(Spectator spectator) {
        Spectator spectator1 = korisnikRep.getOne(spectator.getId());

        return korisnikRep.save(spectator1);
    }


}
