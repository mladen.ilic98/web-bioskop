package com.example.demo.Controller;

import com.example.demo.Entity.Korisnik;
import com.example.demo.Entity.Movie;
import com.example.demo.Entity.Search;
import com.example.demo.Entity.Spectator;
import com.example.demo.Service.KorisnikService;
import com.example.demo.Service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@Controller
public class KorisnikController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private MovieService movieService;

    public Long spectatorId;

    @GetMapping("/login")
    public String getLogin(Model model) {
        Korisnik korisnik = new Korisnik();
        model.addAttribute("korisnik", korisnik);
        return "login.html";
    }

    @PostMapping(value="/login")
    public String login(@ModelAttribute Korisnik korisnik) {
        Spectator spectator = this.korisnikService.findByUsernameAndPassword(korisnik.getUsername(), korisnik.getPassword());
        if (spectator != null) {
            return "redirect:/spectator/"+spectator.getId();
        }
        return "redirect:/login";
    }

    @GetMapping(value = "/spectator/{id}")
    public String spectator(@PathVariable(name = "id") Long id,Model model){
        spectatorId = id;
        Spectator spectator = this.korisnikService.findById(id);
        List<Movie> reservedMovies = spectator.getReservedMovies();
        List<Movie> acceptedResrvations = spectator.getAcceptedReservtions();
        model.addAttribute("movies", movieService.findAll());
        model.addAttribute("search", new Search());
        model.addAttribute("spectator", spectator);
        model.addAttribute("reserve", reservedMovies);
        model.addAttribute("accept", acceptedResrvations);

        return "spectator.html";
    }

    @GetMapping(value = "/profile/{id}")
    public String spectatorProfile(@PathVariable(name = "id") Long id,Model model){
        Spectator s = korisnikService.findById(id);

        model.addAttribute("spectator", s);

        return "profile.html";
    }


    @GetMapping("/logout")
    public String logout() {
        return "redirect:/";
    }

    @GetMapping("/register")
    public String registration(Model model) {
        Spectator spectator = new Spectator();
        model.addAttribute("spectator", spectator);
        return "register.html";
    }

    @PostMapping(value = "/register")
    public String addUser(
            @RequestParam String username,
            @RequestParam String name,
            @RequestParam String surname,
            @RequestParam String password,
            @RequestParam String email,

            @RequestParam String phone,
            @RequestParam String dateOB
    ) {
        try {
            Spectator spectator = new Spectator();
            spectator.setUsername(username);
            spectator.setName(name);
            spectator.setSurname(surname);
            spectator.setPassword(password);
            spectator.setEmail(email);

            spectator.setPhone(phone);
            spectator.setDateOB(dateOB);

            if(this.korisnikService.registration(spectator)==null){
                return "fail.html";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "home.html";
    }

    public boolean chechusername(String username) {
        Spectator spectator = korisnikService.findForRegistration(username);

        if (spectator == null) {
            return true;
        }
        return false;
    }

    @PostMapping("/savespectator")
    public String saveSpectator(@Valid @ModelAttribute Spectator spectator) throws Exception {
        if (chechusername(spectator.getUsername()) == true) {
            this.korisnikService.registration(spectator);
            return "redirect:/login";
        }
        return "redirect:/register";
    }

    @GetMapping(value = "/reserve/{id}")
    public String reserveMovie(@PathVariable(name = "id") Long id){
        Movie movie = movieService.findById(id);
        Spectator spectator = korisnikService.findById(spectatorId);
        List<Movie> reservedMovies = spectator.getReservedMovies();
        reservedMovies.add(movie);
        spectator.setReservedMovies(reservedMovies);
        korisnikService.updateSpectaor(spectator);
        return "redirect:/spectator/" + spectatorId;
    }

    @GetMapping(value = "/accept/{id}")
    public String accept(@PathVariable(name = "id") Long id){
        Movie movie = movieService.findById(id);
        Spectator spectator = korisnikService.findById(spectatorId);
        List<Movie> acceptedReservtions = spectator.getAcceptedReservtions();
        List<Movie> reservedMovies = spectator.getReservedMovies();
        acceptedReservtions.add(movie);
        reservedMovies.remove(movie);
        spectator.setReservedMovies(reservedMovies);
        spectator.setAcceptedReservtions(acceptedReservtions);
        korisnikService.updateSpectaor(spectator);
        return "redirect:/spectator/" + spectatorId;
    }

    @GetMapping(value = "/decline/{id}")
    public String decline(@PathVariable(name = "id") Long id){
        Movie movie = movieService.findById(id);
        Spectator spectator = korisnikService.findById(spectatorId);
        List<Movie> reservedMovies = spectator.getReservedMovies();
        reservedMovies.remove(movie);
        spectator.setReservedMovies(reservedMovies);
        korisnikService.updateSpectaor(spectator);
        return "redirect:/spectator/" + spectatorId;
    }

    @GetMapping("/specator/movie/sortNameOMUp")
    public String sortByNameUp( Model model) {


        List<Movie> movies = this.movieService.sortByNameUp();
        Spectator spectator = this.korisnikService.findById(spectatorId);
        List<Movie> reservedMovies = spectator.getReservedMovies();
        List<Movie> acceptedResrvations = spectator.getAcceptedReservtions();
        model.addAttribute("movies", movieService.findAll());
        model.addAttribute("search", new Search());
        model.addAttribute("spectator", spectator);
        model.addAttribute("reserve", reservedMovies);
        model.addAttribute("accept", acceptedResrvations);
        return "spectator.html";
    }

    @GetMapping("/specator/movie/sortNameOMDown")
    public String sortByNameDown( Model model) {


        List<Movie> movies = this.movieService.sortByNameDown();
        Spectator spectator = this.korisnikService.findById(spectatorId);
        List<Movie> reservedMovies = spectator.getReservedMovies();
        List<Movie> acceptedResrvations = spectator.getAcceptedReservtions();
        model.addAttribute("movies", movies);
        model.addAttribute("search", new Search());
        model.addAttribute("spectator", spectator);
        model.addAttribute("reserve", reservedMovies);
        model.addAttribute("accept", acceptedResrvations);
        return "spectator.html";
    }

    @GetMapping("/specator/movie/sortPriceUp")
    public String sortByPriceUp( Model model) {


        List<Movie> movies = this.movieService.sortByPriceUp();
        Spectator spectator = this.korisnikService.findById(spectatorId);
        List<Movie> reservedMovies = spectator.getReservedMovies();
        List<Movie> acceptedResrvations = spectator.getAcceptedReservtions();
        model.addAttribute("movies", movies);
        model.addAttribute("search", new Search());
        model.addAttribute("spectator", spectator);
        model.addAttribute("reserve", reservedMovies);
        model.addAttribute("accept", acceptedResrvations);
        return "spectator.html";
    }

    @GetMapping("/specator/movie/sortPriceDown")
    public String sortByPriceDown( Model model) {


        List<Movie> movies = this.movieService.sortByPriceDown();
        Spectator spectator = this.korisnikService.findById(spectatorId);
        List<Movie> reservedMovies = spectator.getReservedMovies();
        List<Movie> acceptedResrvations = spectator.getAcceptedReservtions();
        model.addAttribute("movies", movies);
        model.addAttribute("search", new Search());
        model.addAttribute("spectator", spectator);
        model.addAttribute("reserve", reservedMovies);
        model.addAttribute("accept", acceptedResrvations);
        return "spectator.html";
    }

    @PostMapping("/specator/movie/search")
    public String search(@ModelAttribute Search search2,Model model){
        String search = search2.getSearch();

        List<Movie> movies=movieService.findAll();
        List<Movie> mov = new ArrayList<>();
        for(Movie m : movies){
            if(m.getNameOM().toUpperCase().contains(search.toUpperCase()) || Double.toString(m.getPrice()).toUpperCase().contains(search.toUpperCase()) || m.getDuration().toUpperCase().contains(search.toUpperCase()) || Double.toString(m.getMark()).toUpperCase().contains(search.toUpperCase()) || m.getDescription().toUpperCase().contains(search.toUpperCase()) || m.getGenre().toUpperCase().contains(search.toUpperCase())){
                mov.add(m);
            }
        }
        Spectator spectator = this.korisnikService.findById(spectatorId);
        List<Movie> reservedMovies = spectator.getReservedMovies();
        List<Movie> acceptedResrvations = spectator.getAcceptedReservtions();
        model.addAttribute("search", new Search());
        model.addAttribute("spectator", spectator);
        model.addAttribute("reserve", reservedMovies);
        model.addAttribute("accept", acceptedResrvations);
        model.addAttribute("movies", mov);
        return "spectator.html";
    }
}
