package com.example.demo.Controller;

import com.example.demo.Entity.Movie;

import com.example.demo.Entity.Search;
import com.example.demo.Service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MovieController {
    @Autowired
    MovieService movieService;

    @GetMapping(value = "/")
    public String welcome(Model model){
        model.addAttribute("movie", movieService.findAll());
        model.addAttribute("search", new Search());
        return "movie";
    }

    @GetMapping("/movie/sortNameOMUp")
    public String sortByNameUp( Model model) {


        List<Movie> movies = this.movieService.sortByNameUp();

        model.addAttribute("movie", movies);
        model.addAttribute("search", new Search());
        return "movie";
    }

    @GetMapping("/movie/sortNameOMDown")
    public String sortByNameDown( Model model) {


        List<Movie> movies = this.movieService.sortByNameDown();

        model.addAttribute("movie", movies);
        model.addAttribute("search", new Search());
        return "movie.html";
    }

    @GetMapping("/movie/sortPriceUp")
    public String sortByPriceUp( Model model) {


        List<Movie> movies = this.movieService.sortByPriceUp();

        model.addAttribute("movie", movies);
        model.addAttribute("search", new Search());
        return "movie.html";
    }

    @GetMapping("/movie/sortPriceDown")
    public String sortByPriceDown( Model model) {


        List<Movie> movies = this.movieService.sortByPriceDown();

        model.addAttribute("movie", movies);
        model.addAttribute("search", new Search());
        return "movie.html";
    }

    @PostMapping("/movie/search")
    public String search(@ModelAttribute Search search2,Model model){
        String search = search2.getSearch();

        List<Movie> movies=movieService.findAll();
        List<Movie> mov = new ArrayList<>();
        for(Movie m : movies){
            if(m.getNameOM().toUpperCase().contains(search.toUpperCase()) || Double.toString(m.getPrice()).toUpperCase().contains(search.toUpperCase()) || m.getDuration().toUpperCase().contains(search.toUpperCase()) || Double.toString(m.getMark()).toUpperCase().contains(search.toUpperCase()) || m.getDescription().toUpperCase().contains(search.toUpperCase()) || m.getGenre().toUpperCase().contains(search.toUpperCase())){
                mov.add(m);
            }
        }
        model.addAttribute("movie", mov);
        model.addAttribute("search", new Search());
        return "movie";
    }


}
