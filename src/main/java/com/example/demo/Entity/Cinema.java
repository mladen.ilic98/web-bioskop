package com.example.demo.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cinema {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name= "nameOC")
    private String nameOC;

    @Column(name="adress")
    private String adress;

    @Column(name="numberOP")
    private String numberOP;

    @Column(name = "email")
    private String email;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameOC() {
        return nameOC;
    }

    public void setNameOC(String nameOC) {
        this.nameOC = nameOC;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getNumberOP() {
        return numberOP;
    }

    public void setNumberOP(String numberOP) {
        this.numberOP = numberOP;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }




}
