package com.example.demo.Entity;

import javax.persistence.Entity;


public class Search {
    private String search;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Search() { search = new String();}
}
