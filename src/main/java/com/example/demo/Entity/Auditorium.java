package com.example.demo.Entity;

import javax.persistence.*;

@Entity
public class Auditorium {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="capacity")
    private Integer capacity;

    @Column(name="markOA")
    private String markOA;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getMarkOA() {
        return markOA;
    }

    public void setMarkOA(String markOA) {
        this.markOA = markOA;
    }
}
