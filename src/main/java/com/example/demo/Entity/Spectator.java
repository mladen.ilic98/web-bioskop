package com.example.demo.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Spectator extends Korisnik{

    @ManyToMany
    private List<Movie> reservedMovies = new ArrayList<>();

    @ManyToMany
    private List<Movie> acceptedReservtions = new ArrayList<>();

    public List<Movie> getReservedMovies() {
        return reservedMovies;
    }

    public void setReservedMovies(List<Movie> reservedMovies) {
        this.reservedMovies = reservedMovies;
    }

    public List<Movie> getAcceptedReservtions() {
        return acceptedReservtions;
    }

    public void setAcceptedReservtions(List<Movie> acceptedReservtions) {
        this.acceptedReservtions = acceptedReservtions;
    }
}
