package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication

public class WebBioskopApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebBioskopApplication.class, args);
	}

}
